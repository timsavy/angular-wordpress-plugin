import { Component, OnInit } from '@angular/core';
import { WordpressService } from 'src/app/services/wordpress.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.scss']
})
export class FileUploaderComponent implements OnInit {

  mediaForm: FormGroup;
  file: File;
  imageUrl: any;

  constructor(
    private wp: WordpressService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.mediaForm = this.fb.group({
      file: null
    });
  }

  uploadFile() {
    if (this.file) {
      this.wp.uploadFile(this.file).toPromise().then(res => {
        console.log(res);
      });
    }
  }

  readyImage(e: any) {
      let reader = new FileReader();
      this.file = e.target.files[0];
      
      reader.onloadend = () => {
        this.imageUrl = reader.result;
      }
      reader.readAsDataURL(this.file);
  }

}
