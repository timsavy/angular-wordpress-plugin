import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploaderComponent } from './file-uploader/file-uploader.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PostListComponent } from './post-list/post-list.component';

@NgModule({
  declarations: [
    FileUploaderComponent,
    PostListComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    FileUploaderComponent,
    PostListComponent
  ]
})
export class ComponentsModule { }
