import { Component, OnInit, Inject } from '@angular/core';
import { WordpressService } from '../services/wordpress.service';
import { Observable, Subscription } from 'rxjs';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  posts$: Observable<any>;
  mediaForm: FormGroup;
  file: File;
  imageUrl: any;

  constructor(
    private wp: WordpressService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.posts$ = this.wp.getPosts('floorplan');
  }

  createPost() {
    this.wp.createPost('posts', 'First Test', 'This is a test').toPromise().then(res => {
      console.log(res);
    })
  }
  

}
