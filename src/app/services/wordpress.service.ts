import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WordpressService {

  constructor(
    private http: HttpClient,
    @Inject('WINDOW') private window: any
  ) { }


  getPosts(type: string): Observable<any> {
    return this.http.get<any>(`${this.window.app_data.root}/${type}?_embed`);
  }

  createPost(type: string, title: string, content: string) {
    const url = `${this.window.app_data.root}/${type}`;
    const headers =  {
      headers: new HttpHeaders().set('X-WP-Nonce', this.window.app_data.nonce)
    }
    const post = {
      title: title,
      content: content,
      status: 'publish'
    }
    return this.http.post(url, post, headers).pipe(
      map(res => {
        return res;
      })
    );
  }

  uploadFile(file) {
		var formData = new FormData();
		formData.append( 'file', file );
		formData.append( 'title', file.name );
    const url = `${this.window.app_data.media}`;
    const headers =  {
      headers: new HttpHeaders().set('X-WP-Nonce', this.window.app_data.nonce)
    }
    return this.http.post(url, formData, headers).pipe(
      map(res => {
        return res;
      })
    );
  }

}
