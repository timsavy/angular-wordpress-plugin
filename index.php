<?php 
/*
Plugin Name: Angular Plugin
Plugin URI: https://timduddy.com
Description: Boilerplate for creating a WordPress plugin with Angular
Author: Tim Duddy
Version: 1.0.0
Author URI: https://timduddy.com
*/


class AngularPlugin {

    function __construct() {
        add_action( 'admin_menu', array($this, 'init'), 99, 0 );
    }
    
    public function slug() {
        return 'angular-plugin';
    }

    public function init() {
        add_menu_page( 'Angular Plugin Page', 'Angular Plugin', 'manage_options', $this->slug(), array( $this, 'display' ) );
    }

    public function display() {
        $this->include_angular_app(plugin_dir_url( __FILE__ ) . 'dist');
    }
    
    public function include_angular_app( $dist_path ) {
        $i = 0;
        foreach ( glob( plugin_dir_path( __FILE__ ) . 'dist/*.js' ) as $file ) {
            $filename = str_replace(plugin_dir_path( __FILE__ ) . 'dist/', '', $file );
            wp_enqueue_script( 'angular_script_' . $i, plugin_dir_url( __FILE__ ) . 'dist/' . $filename );
            $i++;
        }
        wp_localize_script( 'angular_script_0', 'app_data', array(
            'root' => esc_url_raw( rest_url( '/wp/v2' ) ),
            'nonce' => wp_create_nonce( 'wp_rest' ),
            'media' => esc_url_raw( rest_url( '/wp/v2/media' ) ),
        ) );
        echo '<base href="'. admin_url( 'admin.php?page='.$this->slug() ) .'">';
        echo '<app-root></app-root>';
    }
    
}

new AngularPlugin();


